Auf Router R1:
access-list 100 permit ip 172.26.3.0 0.0.0.7 172.26.3.8 0.0.0.7

crypto isakmp policy 10
 encryption aes 256
 authentication pre-share
 group 5

crypto isakmp key secretkey address 172.26.3.22

crypto ipsec transform-set R1->R2 esp-aes 256 esp-sha-hmac

crypto map IPSEC-MAP 10 ipsec-isakmp 
 set peer 172.26.3.22
 set pfs group5
 set security-association lifetime seconds 86400
 set transform-set R1->R2 
 match address 5

int g0/1
crypto map IPSEC-MAP

Auf Router R2:
access-list 100 permit ip 172.26.3.8 0.0.0.7 172.26.3.0 0.0.0.7

crypto isakmp policy 10
 encryption aes 256
 authentication pre-share
 group 5

crypto isakmp key secretkey address 172.26.3.17

crypto ipsec transform-set R2->R1 esp-aes 256 esp-sha-hmac

crypto map IPSEC-MAP 10 ipsec-isakmp 
 set peer 172.26.3.17
 set pfs group5
 set security-association lifetime seconds 86400
 set transform-set R2->R1 
 match address 5

int g0/0
crypto map IPSEC-MAP

Änderung speichern: 
en 
copy running-config startup-config

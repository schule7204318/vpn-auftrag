1. access-list 100 permit ip 172.26.3.0 0.0.0.7 172.26.3.8 0.0.0.7
Dieser Befehl erstellt eine Zugriffsliste mit der Nummer 100 und erlaubt den IP-Verkehr von einem Netzwerk mit der Adresse 172.26.3.0/29 zu einem anderen Netzwerk mit der Adresse 172.26.3.8/29. 
"permit" bedeutet in diesem Fall "erlauben".

-----------------------------------------------------------------------------------------------------------------

2. crypto isakmp policy 10
 	encryption aes 256
 	authentication pre-share
 	group 5
Dieser Befehl legt die ISAKMP-Richtlinie mit der Nummer 10 fest und konfiguriert die Verschlüsselung mit AES-256, 
die Authentifizierung mit vorab geteiltem Schlüssel und die Diffie-Hellman-Gruppe 5 für die Schlüsselaushandlung. 
"crypto" steht für die kryptografische Konfiguration, 
"isakmp" ist das Protokoll für die Schlüsselaustausch-Authentifizierung, 
"policy" bedeutet "Richtlinie", 
"encryption" steht für Verschlüsselung, 
"authentication" für Authentifizierung und 
"group" für die Gruppe der Diffie-Hellman-Schlüsselaustauschparameter.

-----------------------------------------------------------------------------------------------------------------

3. crypto isakmp key secretkey address 172.26.3.22
Dieser Befehl konfiguriert einen gemeinsamen geheimen Schlüssel ("secretkey") für die Authentifizierung zwischen zwei Geräten. 
Dabei wird die IP-Adresse 172.26.3.22 als Gegenstelle definiert, mit der dieser gemeinsame Schlüssel verwendet wird. 
"crypto" steht für die kryptografische Konfiguration, 
"isakmp" ist das Protokoll für die Schlüsselaustausch-Authentifizierung,
"key" steht für den Schlüssel, 
"address" für die IP-Adresse.
-----------------------------------------------------------------------------------------------------------------

4. crypto ipsec transform-set R1->R2 esp-aes 256 esp-sha-hmac
Dieser Befehl definiert einen Satz von IPSec-Transformationen mit dem Namen "R1->R2". 
Dabei wird die Verschlüsselung mit AES-256 und die Integritätssicherung mit HMAC-SHA1 für das Encapsulating Security Payload (ESP) konfiguriert. 
"crypto" steht für die kryptografische Konfiguration, 
"ipsec" ist das Protokoll für die Sicherung von IP-Paketen, 
"transform-set" bedeutet "Transformationssatz", 
"esp" steht für das Encapsulating Security Payload-Protokoll, 
"aes 256" definiert die AES-256-Verschlüsselung und 
"esp-sha-hmac" definiert die HMAC-SHA1-Integritätssicherung.

-----------------------------------------------------------------------------------------------------------------

5. crypto map IPSEC-MAP 10 ipsec-isakmp 
 	set peer 172.26.3.22
 	set pfs group5
 	set security-association lifetime seconds 86400
 	set transform-set R1->R2 
 	match address 5
Dieser Befehl erstellt eine Crypto-Map mit der Nummer 10 mit dem Namen "IPSEC-MAP". 
Dabei wird IPsec mit ISAKMP zur Sicherung von IP-Paketen verwendet. 
Die Gegenstelle wird mit der IP-Adresse 172.26.3.22 festgelegt. 
Der Diffie-Hellman-Gruppe 5 wird für die Schlüsselaushandlung verwendet. 
Die Lebensdauer der Sicherheitsvereinbarung wird auf 86400 Sekunden festgelegt. 
Der IPSec-Transformationssatz "R1->R2" wird verwendet. 
Schließlich wird mit dem Befehl "match address 5" definiert, dass die Sicherheitsrichtlinie auf die ACL mit der Nummer 5 angewendet wird. 
"crypto" steht für die kryptogra
fische Konfiguration, 
"map" bedeutet "Kartenanzeige", 
"ipsec-isakmp" ist das Protokoll für die Sicherung von IP-Paketen in Kombination mit ISAKMP, 
"peer" steht für Gegenstelle, "pfs" für Perfect Forward Secrecy (PFS), 
"security-association" für Sicherheitsvereinbarung, 
"lifetime" für Lebensdauer, 
"transform-set" für Transformationssatz und 
"match address" für die Übereinstimmung mit einer Zugriffsliste (ACL).

-----------------------------------------------------------------------------------------------------------------

6. int g0/1
   crypto map IPSEC-MAP
Dieser Befehl gilt für die Konfiguration der Schnittstelle "GigabitEthernet 0/1". 
Dabei wird die Crypto-Map "IPSEC-MAP" auf diese Schnittstelle angewendet. 
"int" steht für Schnittstelle ("interface"), 
"g0/1" ist die Abkürzung für GigabitEthernet 0/1 und 
"crypto map" wird verwendet, um eine Crypto-Map auf eine Schnittstelle anzuwenden.

-----------------------------------------------------------------------------------------------------------------
